port module Main exposing (main)

import Html exposing (Html, text)
import Bootstrap.Grid exposing (..)

import GitlabWorkflow.Models exposing (..)
import GitlabWorkflow.Messages
import GitlabWorkflow.Update
import GitlabWorkflow.Views
import Issues.Messages
import Issues.Update
import Issues.Views

main : Program (Maybe Storage) Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : Maybe Storage -> (Model, Cmd Msg)
init mst = case mst of
    Just st -> ( Model [] [] "ready" st.gitlabInfo, Cmd.none)
    Nothing -> ( Model [] [] "missing Gitlab info" emptyGitlabInfo, Cmd.none)

-- Ports

type alias Storage =
    { gitlabInfo : GitlabInfo
    }

port setStorage : Storage -> Cmd msg

store : Model -> Cmd Msg
store model =
    setStorage (Storage model.gitlabInfo)

-- UPDATE

type Msg
    = IssuesMsg Issues.Messages.Msg
    | GitlabMsg GitlabWorkflow.Messages.Msg

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        IssuesMsg msg ->
            let
                (data, cmd) = Issues.Update.update msg model
            in
                (data, Cmd.map IssuesMsg cmd)

        GitlabMsg msg ->
            let
                (gi, cmd) = GitlabWorkflow.Update.update msg model.gitlabInfo
                model_ = {model | gitlabInfo = gi}
            in
                (model_, Cmd.batch [Cmd.map GitlabMsg cmd, store model_])

-- VIEW

view : Model -> Html Msg
view model =
    containerFluid
        [ row
            [ Html.h1 [] [text "Gitlab issues"]
            , Html.map GitlabMsg (GitlabWorkflow.Views.viewAll model.gitlabInfo)
            , Html.map IssuesMsg (Issues.Views.viewAll model)
            ]
        ]

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none
