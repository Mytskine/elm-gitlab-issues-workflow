module GitlabWorkflow.Update exposing (update)

import GitlabWorkflow.Models exposing (..)
import GitlabWorkflow.Messages exposing (..)

update : Msg -> GitlabInfo -> (GitlabInfo, Cmd Msg)
update msg gi =
    case msg of
        GotPrivateToken pt ->
            ({ gi | privateToken = Just pt }, Cmd.none)

        GotProjectId value ->
            let
                id = String.toInt value |> Result.toMaybe
            in
                ({ gi | projectId = id }, Cmd.none)

        AddIssueState ->
            let
                states = gi.issueStates ++ [emptyIssueState]
            in
                ({ gi | issueStates = states }, Cmd.none)

        RemoveIssueState rank ->
            let
                states = removeElement gi.issueStates rank
            in
                ({ gi | issueStates = states }, Cmd.none)

        MoveLeftIssueState rank ->
            let
                states = shiftElementLeft gi.issueStates rank
            in
                ({ gi | issueStates = states }, Cmd.none)

        MoveRightIssueState rank ->
            let
                states = shiftElementRight gi.issueStates rank
            in
                ({ gi | issueStates = states }, Cmd.none)

        GotStateName rank value ->
            let
                state = get rank gi.issueStates |> Maybe.withDefault emptyIssueState
                state_ = { state | name = value }
                states = replace gi.issueStates rank state_
            in
                ({ gi | issueStates = states }, Cmd.none)

        GotStateLabel rank value ->
            let
                state = get rank gi.issueStates |> Maybe.withDefault emptyIssueState
                state_ = { state | label = value }
                states = replace gi.issueStates rank state_
            in
                ({ gi | issueStates = states }, Cmd.none)

get : Int -> List a -> Maybe a
get n xs = List.head (List.drop n xs)

replace : List a -> Int -> a -> List a
replace l rank new =
    let
        replaceOne i x = if i == rank then new else x
    in
        List.indexedMap replaceOne l

removeElement : List a -> Int -> List a
removeElement l i =
    (List.take i l) ++ (List.drop (i+1) l)

justGetIS : Int -> List IssueState -> IssueState
justGetIS n l = Maybe.withDefault emptyIssueState (get n l)

shiftElementLeft : List IssueState -> Int -> List IssueState
shiftElementLeft l i =
    (List.take (i-1) l) ++ (justGetIS i l) :: (justGetIS (i-1) l) :: (List.drop (i+1) l)

shiftElementRight : List IssueState -> Int -> List IssueState
shiftElementRight l i =
    let
        lastRank = List.length l - 1
    in
        (List.take i l ) ++ (justGetIS (i+1) l) :: (justGetIS i l) :: (List.drop (i+2) l)
