module GitlabWorkflow.Messages exposing (..)

type Msg
    = GotPrivateToken String
    | GotProjectId String
    | GotStateName Int String
    | GotStateLabel Int String
    | AddIssueState
    | MoveLeftIssueState Int
    | MoveRightIssueState Int
    | RemoveIssueState Int
