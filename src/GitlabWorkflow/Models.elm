module GitlabWorkflow.Models exposing (..)

import Issues.Models exposing (..)

type alias Model =
    { issues : List Issue
    , groupedIssues : List IssuesGroup
    , networkStatus : String
    , gitlabInfo : GitlabInfo
    }

type alias PrivateToken = String
type alias ProjectId = Int
type alias IssueLabel = String

type alias SortAsc = Bool
type alias SortOrder = List (IssueLabel, SortAsc)

type alias IssueState =
    { name : String
    , label : IssueLabel
    , sort :  SortOrder
    }

type alias IssuesGroup =
    { state : IssueState
    , issues : List Issue
    }

-- Will be stored through a port, so some types are prohibited here.
-- Use:
-- Ints, Floats, Bools, Strings, Maybes, Lists, Arrays, Tuples, Json.Values,
--   and concrete records.
type alias GitlabInfo =
    { privateToken : Maybe PrivateToken
    , projectId : Maybe Int
    , issueStates : List IssueState
    }

emptyIssueState : IssueState
emptyIssueState = IssueState "" "" []

emptyGitlabInfo : GitlabInfo
emptyGitlabInfo = GitlabInfo Nothing Nothing []
