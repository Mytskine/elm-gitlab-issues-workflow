module GitlabWorkflow.Views exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Forms exposing (..)

import GitlabWorkflow.Messages exposing (..)
import GitlabWorkflow.Models exposing (..)

viewAll : GitlabInfo -> Html Msg
viewAll gitlabInfo =
    Bootstrap.Forms.form FormHorizontal [class "well"]
        [ formGroup FormGroupDefault
            [ formLabel [class "col-xs-5 col-sm-2 col-lg-1"] [text "Private token"]
            , div [class "col-xs-7 col-sm-2"]
                [ formInput
                    [ type_ "text"
                    , placeholder "private token"
                    , value <| Maybe.withDefault "" gitlabInfo.privateToken
                    , onInput GotPrivateToken
                    ]
                    []
                ]
            ]
        , formGroup FormGroupDefault
            [ formLabel [class "col-xs-5 col-sm-2 col-lg-1"] [text "Project ID"]
            , div [class "col-xs-7 col-sm-2"]
                [ formInput
                    [ type_ "text"
                    , placeholder "project #"
                    , value <| Maybe.withDefault "" <| Maybe.map toString gitlabInfo.projectId
                    , onInput GotProjectId
                    ]
                    []
                ]
            ]
        , formGroup FormGroupDefault
            [ formLabel [class "col-xs-5 col-sm-2 col-lg-1"] [text "State"]
            , div [] (List.indexedMap viewIssueState gitlabInfo.issueStates)
            , button [type_ "button", class "btn", onClick AddIssueState] [text "Add state"]
            ]
        ]

viewIssueState : Int -> IssueState -> Html Msg
viewIssueState rank state =
    div [class "col-xs-4 col-sm-2 col-lg-1"]
        [ formInput
            [ type_ "text"
            , placeholder "name"
            , value state.name
            , onInput (GotStateName rank)
            ]
            []
        , formInput
            [ type_ "text"
            , placeholder "label"
            , value state.label
            , onInput (GotStateLabel rank)
            ]
            []
        , button [class "btn btn-default", type_ "button", onClick (MoveLeftIssueState rank)] [text "←"]
        , button [class "btn btn-danger", type_ "button", onClick (RemoveIssueState rank)] [text "Remove"]
        , button [class "btn btn-default", type_ "button", onClick (MoveRightIssueState rank)] [text "→"]
        ]
