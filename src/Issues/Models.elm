module Issues.Models exposing (..)

type alias User =
    { id: Int
    , name : String
    , username : String
    , state : String
    , web_url : Maybe String
    }

type alias Issue =
    { id : Int
    , state : String
    , title : String
    , description : String
    , author : User
    , project_id : Int
    -- , milestone : Milestone
    , assignee : Maybe User
    , created_at : String -- date "2016-01-04T15:31:51.081Z",
    , updated_at : Maybe String -- date "2016-01-04T15:31:51.081Z"
    , iid : Int
    , labels : List String
    , subscribed : Bool
    , user_notes_count : Int
    , due_date : Maybe String
    , web_url : Maybe String
    , confidential : Bool
    , weight : Maybe Int
    }
