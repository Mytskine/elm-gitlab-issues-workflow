module Issues.Views exposing (..)

import Html exposing (..)
import Html.Attributes exposing (id, class, type_, placeholder, value)
import Html.Events exposing (..)

import GitlabWorkflow.Models exposing (..)
import Issues.Models exposing (..)
import Issues.Messages exposing (..)


viewAll : Model -> Html Msg
viewAll model =
    div []
        [ button [ class "btn btn-primary btn-lg", onClick FetchIssues ] [ text "Read issues" ]
        , div [id "network-status"] [text model.networkStatus]
        , div [id "workflow"] (List.map viewGroup model.groupedIssues)
        ]

viewGroup : IssuesGroup -> Html Msg
viewGroup g =
    section []
        [ h2 [] [text g.state.name]
        , div [] (List.map viewIssue g.issues)
        ]

viewIssue : Issue -> Html Msg
viewIssue i = Html.article [class "issue"]
    [ Html.header [] [text i.title]
    , Html.div [class "state"] [text i.state]
    , Html.div [class "labels"] (List.map viewLabel i.labels)
    , Html.div [class "notescount"] [text (toString i.user_notes_count)]
    , Html.div [class "description"] [text i.description]
    ]

viewLabel : IssueLabel -> Html Msg
viewLabel label =
    span [class "label label-info"] [text label]
