module Issues.Messages exposing (..)

import Issues.Models exposing (..)
import Http exposing (Error)

type Msg
    = FetchIssues
    | GotIssues (Result Http.Error (List Issue))
