module Issues.Update exposing (update)

import Http
import Json.Decode exposing (bool, int, string, list, nullable, Decoder)
import Json.Decode.Pipeline as JPipe exposing (required, optional)

import Issues.Models exposing (..)
import Issues.Messages exposing (..)

import GitlabWorkflow.Models exposing (..)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        FetchIssues ->
            case (model.gitlabInfo.privateToken, model.gitlabInfo.projectId) of
                (Just t, Just id) ->
                     ({model | networkStatus = "fetching…"}, getIssues t id "?state=opened")
                _ ->
                     ({model | networkStatus = "Missing Gitlab info"}, Cmd.none)

        GotIssues (Ok issues) ->
            let
                groups = groupIssues model.gitlabInfo.issueStates issues
                newmodel = {model | issues = issues, groupedIssues = groups, networkStatus = "ready"}
            in
                (newmodel, Cmd.none)

        GotIssues (Err (Http.BadPayload whatFailed _)) ->
            ({model | issues = [], networkStatus = whatFailed}, Cmd.none)

        GotIssues (Err _) ->
            ({model | issues = [], networkStatus = "Undefined error"}, Cmd.none)

-- HTTP

apiGet : PrivateToken -> String -> Http.Expect a -> Http.Request a
apiGet token path expectResponse =
    Http.request
        { method = "GET"
        , headers =
            [ (Http.header "PRIVATE-TOKEN" token)
            ]
        , url = "https://gitlab.com/api/v3" ++ path
        , body = Http.emptyBody
        , expect = expectResponse
        , timeout = Nothing
        , withCredentials = False
        }

getIssues : PrivateToken -> ProjectId -> String -> Cmd Msg
getIssues token projectId param =
    let
        path = "/projects/" ++ (toString projectId) ++ "/issues" ++ param
        request = apiGet token path (Http.expectJson decodeIssues)
    in
        Http.send GotIssues request

groupIssues : List IssueState -> List Issue -> List IssuesGroup
groupIssues sts issues =
    let
        hasLabel state issue = List.member state.label issue.labels
        partitionIssues states is =
            case states of
                state :: otherStates ->
                    let (match, rest) = List.partition (hasLabel state) is
                    in (IssuesGroup state match) :: (partitionIssues otherStates rest)
                _ -> [IssuesGroup (IssueState "Others" "" []) is]
    in
        List.reverse <| partitionIssues (List.reverse sts) issues


-- decoders

decodeUser : Decoder User
decodeUser =
  JPipe.decode User
    |> required "id" int
    |> required "name" string
    |> required "username" string
    |> required "state" string
    |> optional "web_url" (nullable string) Nothing

decodeIssue : Decoder Issue
decodeIssue =
  JPipe.decode Issue
    |> required "id" int
    |> required "state" string
    |> required "title" string
    |> optional "description" string ""
    |> required "author" decodeUser
    |> required "project_id" int
    -- |> optional "milestone" milestoneDecoder
    |> optional "assignee" (nullable decodeUser) Nothing
    |> required "created_at" string
    |> optional "updated_at" (nullable string) Nothing
    |> required "iid" int
    |> required "labels" (list string)
    |> required "subscribed" bool
    |> required "user_notes_count" int
    |> optional "due_date" (nullable string) Nothing
    |> optional "web_url" (nullable string) Nothing
    |> required "confidential" bool
    |> optional "weight" (nullable int) Nothing

decodeIssues : Decoder (List Issue)
decodeIssues = list decodeIssue
